﻿$string  = "This string is cool. Very cool. It also rocks. Very cool. I mean dude. Very cool. Very cool."
$separator = "Very cool.","."

$option = [System.StringSplitOptions]::RemoveEmptyEntries
#$option = [System.StringSplitOptions]::None
$array = $string.split($separator, $option)
$array.Count

0..($array.Count-1) | % {
    Write-Host $_ $array[$_]
}