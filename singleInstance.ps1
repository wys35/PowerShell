[System.Threading.Mutex]$mutant;

# Obtain a system mutex that prevents more than one deployment taking place at the same time.
[bool]$wasCreated = $false;
$mutant = New-Object System.Threading.Mutex($true, "Global\MyMutex", [ref] $wasCreated);        
if (!$wasCreated)
{            
	Throw "Script running"
}

try {
	Write-Output "do work"
	Start-Sleep 3
	Write-Output "work complete"
} finally {
	$mutant.ReleaseMutex(); 
	$mutant.Close();	
}

