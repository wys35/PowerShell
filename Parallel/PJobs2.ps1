﻿$start = Get-Date

# get all hotfixes
$task1 = { Get-Hotfix }

# get all scripts in your profile
$task2 = { Get-Service | Where-Object Status -eq Running }

# parse log file
$task3 = { Get-Content -Path $env:windir\windowsupdate.log | Where-Object { $_ -like '*successfully installed*' } }

# run them all in the foreground:
$result1 = Invoke-Command -ScriptBlock $task1 
$result2 = Invoke-Command -ScriptBlock $task2 
$result3 = Invoke-Command -ScriptBlock $task3 

$end = Get-Date
Write-Host -ForegroundColor Red ($end - $start).TotalSeconds