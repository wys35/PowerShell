﻿$servers = 
$servers | ForEach-Object {
    Write-Host "checking $_"
    Get-EventLog -ComputerName $_ -LogName System -EntryType Error -After (Get-Date).AddDays(-1) | Format-Table -Wrap -Property MachineName, Index, TimeGenerated, EntryType, Source, InstanceID, Message -AutoSize;

}