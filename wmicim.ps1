﻿#requires -Version 3

# classic approach, always uses DCOM
Get-WmiObject -Class Win32_Processor -ComputerName Chinaapp1

# modern approach, you choose the protocol
# and you can reuse sessions
$option = New-CimSessionOption -Protocol Dcom
$session = New-CimSession -SessionOption $option -ComputerName Chinaapp1 

Get-CimInstance -ClassName Win32_Processor -CimSession $session 

Remove-CimSession -CimSession $session