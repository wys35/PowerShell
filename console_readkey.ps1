﻿while ($true) {
    if ($host.ui.RawUI.KeyAvailable) {
        $key = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyUp")
        if ($key.VirtualKeyCode -eq 81) {
            Write-Host "Q pressed"
            break
        }
    }
}