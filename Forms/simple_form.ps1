﻿Function Button_click () {
    $form.Close()
}
Add-Type -AssemblyName System.Windows.Forms
$form = New-Object System.Windows.Forms.Form
$form.Text = "Eddy's test form"
$form.Size = New-Object System.Drawing.Size(400,400)
$form.StartPosition = "CenterScreen"
$form.FormBorderStyle = "Fixed3D"

$Icon = [System.Drawing.Icon]::ExtractAssociatedIcon($PSHOME + "\powershell.exe")
$form.Icon = $Icon

$font = New-Object System.Drawing.Font("Times New Roman", 18, [System.Drawing.FontStyle]::Italic)
$form.Font = $font

$label = New-Object System.Windows.Forms.Label
$label.Text = "This form is very simple."
$label.AutoSize = $true
$form.Controls.Add($label)

$button = New-Object System.Windows.Forms.Button
$button.Text = "Close"
$button.AutoSize = $true
$button.Left = 260
$button.Top = 20
$button.Add_click({Button_click})


$Image = [system.drawing.image]::FromFile("$PSScriptRoot\tetris.png") 
$picturebox = New-Object Windows.Forms.PictureBox
$picturebox.left = 10
$picturebox.Top = 100
$picturebox.Width = $image.Size.Width
$picturebox.height = $image.Size.Height
$picturebox.image = $image

$form.controls.Add($pictureBox)

$form.Controls.Add($button)
$form.ShowDialog()


